﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

public class RSACryptography
{
    private const int KeySize = 2048;

    public static void GenerateKeys(string privateKeyPath, string publicKeyPath)
    {
        using (var rsa = new RSACryptoServiceProvider(KeySize))
        {
            try
            { 
                var privateKey = rsa.ToXmlString(true);
                File.WriteAllText(privateKeyPath, privateKey);

                var publicKey = rsa.ToXmlString(false);
                File.WriteAllText(publicKeyPath, publicKey);

                Console.WriteLine("RSA keys generated successfully.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error generating RSA keys: {ex.Message}");
            }
        }
    }

    public static string Encrypt(string publicKeyPath, string plaintext)
    {
        using (var rsa = new RSACryptoServiceProvider(KeySize))
        {
            try
            {
                rsa.FromXmlString(File.ReadAllText(publicKeyPath));

                byte[] plaintextBytes = Encoding.UTF8.GetBytes(plaintext);
                byte[] encryptedBytes = rsa.Encrypt(plaintextBytes, true);

                return Convert.ToBase64String(encryptedBytes);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error encrypting message: {ex.Message}");
                return null;
            }
        }
    }

    public static string Decrypt(string privateKeyPath, string encryptedText)
    {
        using (var rsa = new RSACryptoServiceProvider(KeySize))
        {
            try
            {
                rsa.FromXmlString(File.ReadAllText(privateKeyPath));

                byte[] encryptedBytes = Convert.FromBase64String(encryptedText);
                byte[] decryptedBytes = rsa.Decrypt(encryptedBytes, true);

                return Encoding.UTF8.GetString(decryptedBytes);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error decrypting message: {ex.Message}");
                return null;
            }
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        string privateKeyPath = "privateKey.xml";
        string publicKeyPath = "publicKey.xml";
        string encryptedMessagePath = "encryptedMessage.txt";
        string decryptedMessagePath = "decryptedMessage.txt";

        try
        {
            if (!File.Exists(privateKeyPath) || !File.Exists(publicKeyPath))
            {
                RSACryptography.GenerateKeys(privateKeyPath, publicKeyPath);
            }

            Console.WriteLine("Enter text:");
            string plaintext = Console.ReadLine();

            Console.WriteLine("Do you want to encrypt the text? (yes/no)");
            string encryptInput = Console.ReadLine().ToLower();
            if (encryptInput == "yes" || encryptInput == "y")
            {
                string encryptedText = RSACryptography.Encrypt(publicKeyPath, plaintext);
                if (encryptedText != null)
                { Console.WriteLine("Encrypted message: " + encryptedText);
                    File.WriteAllText(encryptedMessagePath, encryptedText);
                }
            }

            Console.WriteLine("Do you want to decrypt the text? (yes/no)");
            string decryptInput = Console.ReadLine().ToLower();
            if (decryptInput == "yes" || decryptInput == "y")
            {
                string encryptedText = File.ReadAllText(encryptedMessagePath);
                string decryptedText = RSACryptography.Decrypt(privateKeyPath, encryptedText);
                if (decryptedText != null)
                {
                    Console.WriteLine("Decrypted message: " + decryptedText);
                    File.WriteAllText(decryptedMessagePath, decryptedText);
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred: {ex.Message}");
        }
    }
}
